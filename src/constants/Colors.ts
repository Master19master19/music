const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';
export const primaryColor = '#00B39B';
export const lightColor = '#E6F5F2';
export const textPrimary = '#00B39B';
export const colorPrimary = '#00B39B';
export const textDark = '#000';

export default {
    light: {
        text: '#000',
        background: '#fff',
        tint: tintColorLight,
        tabIconDefault: '#ccc',
        tabIconSelected: tintColorLight,
    },
    dark: {
        text: '#fff',
        background: '#000',
        tint: tintColorDark,
        tabIconDefault: '#ccc',
        tabIconSelected: tintColorDark,
    },
};
