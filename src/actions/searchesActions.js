import axios from 'axios';
import { updateAllFilters } from './exploreActions/exploreFiltersActions';
import { setLandingSearch } from './exploreActions/exploreMapActions';
import { API_URL } from '@env';

const API = axios.create({
    baseURL: `${API_URL}/searches`,
    withCredentials: true,
});

export const saveSearch = () => async (dispatch, getState) => {
    const {
        explore: {
            exploreFilters,
            exploreMap: { mapRef },
        },
    } = getState();
    const bounds = mapRef.getBounds();
    const ne = bounds.getNorthEast();
    const sw = bounds.getSouthWest();
    const resp = await API.post('/save-search', {
        ...exploreFilters,
        box: [sw.lng(), sw.lat(), ne.lng(), ne.lat()],
    });
    console.log('exploreFilters', exploreFilters);
};

export const getSearches = () => async (dispatch) => {
    const resp = await API.get('/get-searches');
    resp.status == 200 &&
        dispatch({
            type: 'searches/GET_SEARCHES_SUCCESS',
            payload: resp.data.items,
        });
};

export const updateSearch = (data) => async (dispatch) => {
    console.log('updateSearch');
    dispatch({
        type: 'searches/UPDATE_SEARCH_SUCCESS',
        payload: data,
    });
    const resp = await API.post('/update-search', {
        ...data,
    });
};

export const applySearch = (search) => (dispatch) => {
    dispatch(updateAllFilters(search));
    dispatch(
        setLandingSearch({
            geometry: {
                bounds: new google.maps.LatLngBounds(
                    new google.maps.LatLng(search.box[1], search.box[0]),
                    new google.maps.LatLng(search.box[3], search.box[2])
                ),
            },
        })
    );
};
