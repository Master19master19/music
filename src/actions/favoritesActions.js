import axios from 'axios';
import { API_URL } from '@env';

const API = axios.create({
    baseURL: `${API_URL}/favorites`,
    withCredentials: true,
});

export const favoriteListing = (listingId) => async (dispatch) => {
    console.log('favoriteListing');
    const resp = await API.post('/favorite-listing', {
        listingId,
    });
    dispatch({
        type: 'favorites/UPDATE_FAVORITE_ID',
        payload: { listingId },
    });
};

export const getFavoritesIds = () => async (dispatch) => {
    const resp = await API.get('/get-favorites-ids');
    resp.status == 200 &&
        dispatch({
            type: 'favorites/GET_FAVORITES_IDS_SUCCESS',
            payload: resp.data.items,
        });
};

export const getFavoriteListings = () => async (dispatch) => {
    const resp = await API.get('/get-favorites-listings');
    resp.status == 200 &&
        dispatch({
            type: 'favorites/GET_FAVORITES_LISTINGS_SUCCESS',
            payload: resp.data.items,
        });
};
