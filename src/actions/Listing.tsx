import axios from 'axios';

// const getFilters = (filters = {}) => {
//
// });

const setListings = (listings, total, loading) => {
    return {
        type: 'GET_LISTINGS',
        payload: { listings, total, loading },
    };
};

const getListings = (filters = {}) => {
    return async (dispatch, getState) => {
        await dispatch(
            setListings([], getState()?.listingReducer?.total ?? 0, true)
        );
        try {
            let listingUrl = `https://dev.simpleshowing.com/api/listings/from-bounds?start=true`;
            // const query = {
            //     sortBy: 'Newest+Listings',
            //     propertyType: 'All+Property+Types',
            //     hidePending: true,
            //     page: 1,
            //     pageSize: 16,
            //     lat: 33.769147,
            //     lon: -84.365651,
            //     alat: 33.8876179,
            //     blat: 33.6478079,
            //     blng: 84.5518189,
            //     alng: -84.289389,
            //     zoom: 11,
            // };
            const query = getState()?.filterReducer.filters;
            // if (Object.keys(filters).length) {
            //     for (let key in filters) {
            //         if (key === 'geo') {
            //             const geo = filters['geo'];
            //             const viewport = geo?.viewport;
            //             query.lat = geo?.location?.lat;
            //             query.lon = geo?.location?.lng;
            //             query.alat = viewport?.northeast?.lat;
            //             query.alng = viewport?.northeast?.lng;
            //             query.blat = viewport?.southwest?.lat;
            //             query.blng = viewport?.southwest?.lng;
            //         } else {
            //             query[key] = filters[key];
            //         }
            //     }
            // }
            for (let key in query) {
                listingUrl += `&${key}=${query[key]}`;
            }
            // console.log(query);
            console.log(listingUrl);
            let res = await axios.get(listingUrl).catch((error) => {
                console.log(error);
                global.Alert(error.toString());
            });
            await dispatch(
                setListings(res?.data?.items, res?.data?.total, false)
            );
            return res?.data || [];
            // console.log('fuck');
            // const apiReq = await fetch(hardCodedListingMain, {
            //     method: 'GET',
            // });
            // console.log('fuck1');
            // console.log(apiReq.data);
        } catch (error) {
            global.Alert(error.toString());
            console.error(error);
        }
    };
};

export default { getListings };
