import axios from 'axios';
import firebase from 'firebase';
import { API_URL } from '@env';

const API = axios.create({
    baseURL: `${API_URL}/auth`,
    withCredentials: true,
});

export const logout = () => async (dispatch) => {
    await API.get('/logout'); //invalidate session
    firebase.auth().signOut();
    dispatch({
        type: 'users/GET_PROFILE_SUCCESS',
        payload: { user: null },
    });
};

export const authFromToken = (token) => async (dispatch) => {
    const resp = await API.post(
        '/auth-from-token',
        { token },
        { validateStatus: false }
    );

    const { user, location, error } = resp.data;

    user &&
        dispatch({
            type: 'users/GET_PROFILE_SUCCESS',
            payload: { user, location },
        });
    return resp;
};

const getToken = async () => {
    const fbAuth = firebase.auth();
    if (fbAuth && fbAuth.currentUser) {
        const token = await fbAuth.currentUser.getIdToken(true);
        return token;
    }
    return null;
};

const createAccount = (userData, args = {}) => async (dispatch) => {
    const token = await getToken(userData);

    const resp = await API.post('/create-account', { ...args, token });
    const { user, location, error } = resp.data;
    if (error) {
        console.error(error);
        return;
    }

    dispatch({
        type: 'users/GET_PROFILE_SUCCESS',
        payload: { user, location },
    });
};

const login = (userData) => async (dispatch) => {
    const token = await getToken(userData);
    const resp = await dispatch(authFromToken(token));
    const { error, user } = resp.data;
    if (error) {
        console.error(error);
    } else if (user) {
        //success message
    }
};

// FIREBASE

const handleFirebaseError = (err) => {
    console.error('handleFirebaseError', err.message);
};

// check existing accounts
export const hasSignInMethods = (email) => async (dispatch) => {
    const signInMethods = await firebase
        .auth()
        .fetchSignInMethodsForEmail(email);
    return signInMethods.length ? true : false;
};

export const canSignInWithPassword = (email) => async (dispatch) => {
    const signInMethods = await firebase
        .auth()
        .fetchSignInMethodsForEmail(email);
    for (let option of signInMethods) {
        if (option === 'password') return true;
    }
    return false;
};

// Sign up

export const signUpWithPassword = (email, password, args = {}) => async (
    dispatch
) => {
    return firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async (userData) => {
            await dispatch(createAccount(userData, args));
        })
        .catch(handleFirebaseError);
};

export const signUpWithProvider = (provider, args = {}) => async (dispatch) => {
    console.log('signUpWithProvider');
    const authProvider =
        provider === 'facebook'
            ? new firebase.auth.FacebookAuthProvider()
            : new firebase.auth.GoogleAuthProvider();

    return firebase
        .auth()
        .signInWithPopup(authProvider)
        .then(async (userData) => {
            await dispatch(createAccount(userData, args));
        })
        .catch(handleFirebaseError);
};

export const signUpWithNativeProvider = (data, args = {}) => async (
    dispatch
) => {
    const credential =
        data.provider === 'facebook'
            ? firebase.auth.FacebookAuthProvider.credential(data.accessToken)
            : firebase.auth.GoogleAuthProvider.credential(
                  data.idToken,
                  data.accessToken
              );

    firebase
        .auth()
        .signInWithCredential(credential)
        .then(async (userData) => {
            await dispatch(createAccount(userData, args));
        })
        .catch(handleFirebaseError);
};

// Login

export const loginWithProvider = (provider) => async (dispatch) => {
    const authProvider =
        provider === 'facebook'
            ? new firebase.auth.FacebookAuthProvider()
            : new firebase.auth.GoogleAuthProvider();

    return firebase
        .auth()
        .signInWithPopup(authProvider)
        .then(async (userData) => {
            await dispatch(login(userData));
        })
        .catch(handleFirebaseError);
};

export const loginWithNativeProvider = (data) => async (dispatch) => {
    const { accessToken, idToken } = data;

    const credential =
        data.provider === 'facebook'
            ? firebase.auth.FacebookAuthProvider.credential(accessToken)
            : firebase.auth.GoogleAuthProvider.credential(idToken, accessToken);

    firebase
        .auth()
        .signInWithCredential(credential)
        .then(async (userData) => {
            await dispatch(login(userData));
        })
        .catch(handleFirebaseError);
};

export const loginWithPassword = (email, password) => async (dispatch) => {
    console.log('loginWithPassword');
    return await firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(async (userData) => {
            await dispatch(login(userData));
        })
        .catch(handleFirebaseError);
};
