import axios from 'axios';
import { API_URL } from '@env';

const API = axios.create({
    baseURL: `${API_URL}/listings`,
    withCredentials: true,
});

export const getListing = (listingId) => async (dispatch) => {
    console.log('getListing', listingId);
    const resp = await API.get('/from-id', {
        params: {
            listingId,
        },
    });

    resp.status == 200 &&
        dispatch({
            type: 'listings/GET_LISTING_SUCCESS',
            payload: resp.data,
        });
};

export const shareListing = (data) => async (dispatch) => {
    const resp = await API.post('/share', data);
};
