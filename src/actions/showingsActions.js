import _ from 'lodash';
import axios from 'axios';
import { API_URL } from '@env';

const API = axios.create({
    baseURL: `${API_URL}/showings`,
    withCredentials: true,
});

export const bookShowing = (listing, startTime, endTime, comments) => async (
    dispatch
) => {
    const resp = await API.post('/book-showing', {
        listingId: listing._id,
        startTime,
        endTime,
        comments,
    });
    await dispatch(getShowingsIds());
    const { showing } = resp.data;
    return showing;
};

export const getShowingsIds = () => async (dispatch) => {
    const resp = await API.get('/get-showings-ids');
    resp.status == 200 &&
        dispatch({
            type: 'showings/GET_SHOWINGS_IDS_SUCCESS',
            payload: resp.data.items,
        });
};

export const getTours = () => async (dispatch) => {
    const resp = await API.get('/get-tours');
    resp.status == 200 &&
        dispatch({
            type: 'showings/GET_TOURS_SUCCESS',
            payload: resp.data.items,
        });
};
