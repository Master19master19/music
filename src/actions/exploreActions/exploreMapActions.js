import axios from 'axios';
import _ from 'lodash';
import { API_URL } from '@env';

const API = axios.create({
    baseURL: `${API_URL}/pins`,
});

export const setShowMap = (show) => (dispatch) => {
    dispatch({
        type: 'exploreMap/SET_SHOW_MAP',
        payload: show,
    });
};

export const onIdle = (bounds, zoom) => async (dispatch) => {
    dispatch({
        type: 'exploreMap/ON_IDLE',
        payload: {
            bounds,
            zoom,
        },
    });
};

export const setMapRef = (ref) => async (dispatch) => {
    dispatch({
        type: 'exploreMap/SET_MAP_REF',
        payload: ref,
    });
};

export const setMapCenter = (location) => (dispatch, getState) => {
    const ref = getState().explore.exploreMap.mapRef;
    ref && ref.setCenter(location);
};

export const setMapZoom = (zoom) => (dispatch, getState) => {
    const ref = getState().explore.exploreMap.mapRef;
    ref && ref.setZoom(zoom);
};

export const moveBoundsToPins = () => (dispatch, getState) => {
    const { pins, mapRef } = getState().explore.exploreMap;
    if (!mapRef) return;
    const bounds = new google.maps.LatLngBounds();
    Object.keys(pins).map((key) => {
        const pin = pins[key];
        const { lat, lng } = pin.location;
        bounds.extend({ lat, lng });
    });
    // mapRef.fitBounds(bounds);
};

export const getClusterPins = () => async (dispatch, getState) => {
    const { explore } = getState();
    const bounds = explore.exploreMap.bounds;
    const zoom = explore.exploreMap.zoom;

    const response = await API.get(`/cluster`, {
        params: {
            alat: bounds.ne.lat,
            alng: bounds.ne.lng,
            blat: bounds.sw.lat,
            blng: bounds.sw.lng,
            zoom,
            ...explore.exploreFilters,
        },
    });
    const { items } = response.data;
    dispatch({
        type: 'exploreMap/GET_PINS_SUCCESS',
        payload: _.keyBy(items, '_id'),
    });
};

export const clearPins = () => (dispatch) => {
    dispatch({
        type: 'exploreMap/GET_PINS_SUCCESS',
        payload: {},
    });
};

export const goToSearchLocation = ({ result }) => (dispatch, getState) => {
    const { mapRef } = getState().explore.exploreMap;
    dispatch({
        type: 'exploreListings/SET_SEARCH_TEXT',
        payload: result.formatted_address.replace(', USA', ''),
    });

    const bounds = result.geometry.bounds || result.geometry.viewport;
    mapRef.panToBounds(bounds);
    mapRef.fitBounds(bounds);
};

export const setLandingSearch = (landingSearch) => (dispatch) => {
    console.log('setLandingSearch', landingSearch);
    dispatch({
        type: 'exploreMap/SET_LANDING_SEARCH',
        payload: landingSearch,
    });
};

export const toggleMap = () => (dispatch) => {
    dispatch({
        type: 'exploreMap/TOGGLE_MAP',
    });
};

export const updateHoverGeohash = (geohash) => (dispatch) => {
    dispatch({
        type: 'exploreMap/UPDATE_HOVER_GEOHASH',
        payload: geohash,
    });
};
