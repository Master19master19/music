import _ from 'lodash';

export const updatePriceRange = (min, max) => (dispatch) => {
    dispatch(updateFilter('priceMin', min));
    dispatch(updateFilter('priceMax', max));
};

export const updateFilter = (key, value) => (dispatch) => {
    if (value == undefined) value = null;
    console.log('updateFilter', key, value);
    if (key) {
        dispatch({
            type: 'exploreFilters/UPDATE_FILTER',
            payload: {
                key,
                value,
            },
        });
    }
};

export const updateAllFilters = (data) => (dispatch) => {
    console.log('updateAllFilters');
    _.keys(data).map((key) => {
        dispatch(updateFilter(key, data[key]));
    });
};
