import _ from 'lodash';
import axios from 'axios';
import { API_URL } from '@env';

console.log('API_URL', API_URL);
const API = axios.create({
    baseURL: `${API_URL}/listings`,
});

export const getListingsListFromBounds = (params) => async (
    dispatch,
    getState
) => {
    const { explore, users } = getState();
    const userLocation = users.location || {};
    const { bounds, zoom, page, pageSize } = params;
    //if page change set loading true
    dispatch({
        type: 'exploreListings/SET_LOADING',
        payload: true,
    });
    const resp = await API.get('from-bounds', {
        params: {
            ...explore.exploreFilters,
            alat: bounds.ne.lat,
            alng: bounds.ne.lng,
            blat: bounds.sw.lat,
            blng: bounds.sw.lng,
            page,
            pageSize,
            ...userLocation,
        },
    });
    const { items, total } = resp.data;
    dispatch({
        type: 'exploreListings/FETCH_LISTINGS_LIST_SUCCESS',
        payload: {
            items: _.keyBy(items, '_id'),
            total,
            page,
        },
    });
};

export const clearListings = () => (dispatch) => {
    dispatch({
        type: 'exploreListings/CLEAR_LISTINGS',
    });
};

export const clearClickPin = () => async (dispatch) => {
    dispatch({
        type: 'exploreListings/GET_SELECTED_PIN_SUCCESS',
        payload: null,
    });
};

export const getPinFromGeohash = (geohash) => async (dispatch) => {
    console.log('getPinFromGeohash', geohash);
    dispatch(clearClickPin());
    const resp = await API.get('/from-geohash', {
        params: {
            geohash,
        },
    });
    dispatch({
        type: 'exploreListings/GET_SELECTED_PIN_SUCCESS',
        payload: resp.data.listing,
    });
};
