const initialState = {
    favorites: {},
};

const favoritesReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'favorites/UPDATE_FAVORITE_ID':
            let favorites = state.favorites;
            if (state.favorites[payload.listingId]) {
                delete favorites[payload.listingId];
                favorites = {
                    ...favorites,
                };
            } else {
                favorites = {
                    ...favorites,
                    [payload.listingId]: new Date(),
                };
            }
            return {
                ...state,
                favorites,
            };
        case 'favorites/GET_FAVORITES_IDS_SUCCESS':
            return { ...state, favorites: payload };
        case 'favorites/GET_FAVORITES_LISTINGS_SUCCESS':
            return {
                ...state,
                listings: payload,
            };
        default:
            return state;
    }
};

export default favoritesReducer;
