const initialState = {
    redirect: null,
    listing: null,
    relatedListings: {},
};

const listingsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'listings/GET_LISTING_SUCCESS':
            return {
                ...state,
                listing: payload.listing,
                relatedListings: payload.relatedListings,
            };
        default:
            return state;
    }
};

export default listingsReducer;
