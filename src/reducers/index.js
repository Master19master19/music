import { combineReducers } from 'redux';
import loggedReducer from './isLogged';
import users from './usersReducer';
import counterReducer from './counter';
import listingReducer from './listing';
import filterReducer from './filter';
import navigationReducer from './navigation';

const rootReducer = combineReducers({
    users,
    counterReducer,
    listingReducer,
    filterReducer,
    navigationReducer,
});

export default rootReducer;
