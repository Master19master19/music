import moment from 'moment';
const initialState = {
    tours: {},
    showings: {},
    showingsIds: {},
    bookAsUser: null,
};

const showingsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'showings/CREATE_SHOWING_SUCCESS':
            return { ...state, showings };
        case 'showings/GET_TOURS_SUCCESS':
            return { ...state, tours: payload };
        case 'showings/GET_SHOWINGS_SUCCESS':
            return { ...state, showings: payload };
        case 'showings/GET_SHOWINGS_IDS_SUCCESS':
            return { ...state, showingsIds: payload };
        case 'showings/SET_BOOK_AS_USER':
            return { ...state, bookAsUser: payload };
        case 'showings/UPDATE_TOUR_SUCCESS':
            const tourDate = moment(payload.startTime).format('YYYY-MM-DD');
            const tour = state.tours[tourDate];
            if (tour) {
                state.tours[tourDate] = {
                    showings: tour.showings.map((showing) => {
                        if (showing._id === payload._id) {
                            showing = payload;
                        }
                        return showing;
                    }),
                };
            }
            return { ...state, tours: { ...state.tours } };
        default:
            return state;
    }
};

export default showingsReducer;
