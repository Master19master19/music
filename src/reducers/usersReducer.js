const initialState = {
    user: null,
    location: null,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case 'user/GET_GEOLOCATION_SUCCESS':
            return { ...state, location: payload };
        case 'user/GET_PROFILE_SUCCESS':
            return { ...state, user: payload.user, location: payload.location };
        case 'user/UPDATE_PROFILE_SUCCESS':
            return { ...state, user: payload };
        case 'user/UPDATE_EMAIL_FREQUENCY_SUCCESS':
            state.user.searchEmailFrequency = payload;
            return { ...state, user: { ...state.user } };
        default:
            return state;
    }
};
