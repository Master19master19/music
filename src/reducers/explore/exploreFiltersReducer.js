const initialState = {
    priceMin: undefined,
    priceMax: undefined,
    bedsMin: undefined,
    bathsMin: undefined,
    sortBy: 'Newest Listings',
    propertyType: 'All Property Types',
    hidePending: true,
    searchText: null,
};

const ExploreFiltersReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'exploreFilters/UPDATE_FILTER':
            return { ...state, [payload.key]: payload.value };
        default:
            return state;
    }
};

export default ExploreFiltersReducer;
