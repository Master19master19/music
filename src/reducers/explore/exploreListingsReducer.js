const initialState = {
    listings: {},
    page: 1,
    loading: true,
    total: 0,
    selectedPin: null,
    searchText: '',
};

const ExploreListingsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'exploreListings/CLEAR_LISTINGS':
            return {
                ...state,
                total: 0,
                page: 1,
                loading: false,
                listings: {},
            };
        case 'exploreListings/SET_LOADING':
            return { ...state, listings: {}, loading: payload };
        case 'exploreListings/FETCH_LISTINGS_LIST_SUCCESS':
            return {
                ...state,
                listings: payload.items,
                total: payload.total,
                page: payload.page,
                loading: false,
            };
        case 'exploreListings/GET_SELECTED_PIN_SUCCESS':
            return {
                ...state,
                selectedPin: payload,
            };
        case 'exploreListings/SET_SEARCH_TEXT':
            return {
                ...state,
                searchText: payload,
            };
        default:
            return state;
    }
};

export default ExploreListingsReducer;
