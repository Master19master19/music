const initialState = {
    searches: {},
};

const searchesReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'searches/GET_SEARCHES_SUCCESS':
            return { ...state, searches: payload };
        case 'searches/UPDATE_SEARCH_SUCCESS':
            state.searches[payload._id] = payload;
            return { ...state, searches: { ...state.searches } };
        default:
            return state;
    }
};

export default searchesReducer;
