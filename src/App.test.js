import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import axios from 'axios';
import { ToastContainer , toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

axios.defaults.withCredentials = true;

const API_URL = "https://api.action.am/api";


function Alert ( text , type = 'default' ) {
	if ( type === 'error' ) {
		toast.error( text , {
			position: toast.POSITION.BOTTOM_RIGHT
		});
	} else if ( type === 'default' ) {
		toast.info( text , {
			position: toast.POSITION.BOTTOM_RIGHT
		});
	}
}


export default function App() {
  return (
    <Router>
      <div className="container-fluid">
      	<ToastContainer />
        <nav className="navbar navbar-light bg-light">
          <Link className="navbar-brand" to="/">
            <img alt="logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/YouTube_full-color_icon_%282017%29.svg/1200px-YouTube_full-color_icon_%282017%29.svg.png" height="50" />
          </Link>
          <div className="collapse show" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto d-flex flex-row">
              <li className="nav-item active mr-3">
                <Link className="nav-link" to="/">Home</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/vote">Vote</Link>
              </li>
            </ul>
          </div>
        </nav>
        <Switch>
          <Route path="/vote">
            <About />
          </Route>
          <Route path="/">
            <Example />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
class Example extends React.Component {
  constructor ( props ) {
    super( props );
    let str = window.location.search.replace( '?id=' , '' );
    if ( str.length > 2 ) {
      localStorage.setItem( 'accountStr' , str );
    }
    this.timeout = null;
    this.state = {value: '' , img: '' , numKeys: 1 , youtubeUrl: '' , items: [] , statusText: '' , accountTitle: '' };
    this.handleChange = this.handleChange.bind( this );
  }
  async doSearch( $q ) {
    let res = await fetch( `https://www.googleapis.com/youtube/v3/search?key=AIzaSyCy-8e7OsUm_ZGfSNBdqldADDoD3py0nH4&q=${$q}&part=snippet` );
    let resp = await res.json();
    let items = resp.items;
    // console.log(items);
    // return;
    if ( undefined !== items[ 0 ] ) {
      this.setState({ items });
      // for ( let key in items ) {
      //   // this.setState({
      //   //   title: items[ 0 ][ 'snippet' ][ 'title' ],
      //   //   img: items[ 0 ][ 'snippet' ][ 'thumbnails' ][ 'medium' ][ 'url' ],
      //   //   youtubeUrl: items[ 0 ][ 'id' ][ 'videoId' ]
      //   // })
      // }
    } else {
      this.setState({ statusText: 'No matches found' });
      console.log( 'Nothing found' );
    }
  }
  async componentDidMount () {
    let accountStr = localStorage.getItem( 'accountStr' );
    if ( accountStr !== undefined && accountStr !== null && accountStr.length > 2 ) {
      axios.get( `${API_URL}/${accountStr}` )
      .then( function ( response ) {
        localStorage.setItem( 'accountTitle' , response[ 'data' ][ 'title' ] );
      })
      .catch(function (error) {
        console.log({error});
      });
    }
    let accountTitle = localStorage.getItem( 'accountTitle' ) || '';
    if ( accountTitle.length > 5 ) {
      this.setState({
        accountTitle
      });
    }
  }
  handleChange( event ) {
    clearTimeout( this.timeout );
    this.timeout = setTimeout( () => this.doSearch( this.state.value ) , 1000 );
    // this.setState({ numKeys: this.state.numKeys + 1 })
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <div className="container-fluid">
        <h1>{ this.state.accountTitle }</h1>
        <form className="form mt-4">
          <div className="form-group">
            <input className="form-control" placeholder="Search youtube" autoFocus={ true } type="text" value={this.state.value} onChange={this.handleChange} />
            </div>
        </form>
        <div className="row">
          {
            this.state.items.map( ( item , index ) => {
    			return (
    				item[ 'id' ][ 'kind' ] === 'youtube#video'
    				&&
              		<div className="col-6 p-2 h-show-vid mb-2" key={index}>
	                	<div className="border p-2">
		                  <a href={ "https://www.youtube.com/watch?v=" + item[ 'id' ][ 'videoId' ] }  rel="noopener noreferrer" target="_blank">
		                    <img src={ item[ 'snippet' ][ 'thumbnails' ][ 'medium' ][ 'url' ] } height="75" alt="thumbnail" />
		                  </a>
		                  <p className="mt-3 thumb-fs mb-0">{ item[ 'snippet' ][ 'title' ] }</p>
		                  <button className="btn btn-primary btn-block mt-3" onClick={ () => this.setMusic( item ) }>Add to queue</button>
		                </div>
	                </div>
    			)
            })
          }
        </div>
      </div>
    );
  }

  getAccountStr() {
    let str = localStorage.getItem( 'accountStr' );
    // let str = window.location.search.replace( '?id=' , '' );
    return str;
  }
  setMusic ( item ) {
    let data = {
      accountStr: this.getAccountStr(),
      youtube_id: item[ 'id' ][ 'videoId' ],
      title: item[ 'snippet' ][ 'title' ],
      thumbnail_url: item[ 'snippet' ][ 'thumbnails' ][ 'medium' ][ 'url' ],
    };
    axios.post( `${API_URL}/setMusic` ,  data )
    .then(function (response) {
      Alert( 'Music successfully added!' );
      console.log({response});
    })
    .catch(function (error) {

    	Alert( error.response.data[ 0 ] , 'error' );
      console.log({error});
    });
  }
  _onSearchResultsFound(results) {
    console.log(results)
    // Results is an array of retreived search results
    // I use flux, so I dispatch results to an action
    // flux.actions.showSearchResults(results);
  }
}


class About extends React.Component {
  state = {
    items: [],
    accountTitle: ''
  }
  constructor ( props ) {
    super( props );
    this.interval = null;
  }

  getAccountStr() {
    let str = localStorage.getItem( 'accountStr' );
    // let str = window.location.search.replace( '?id=' , '' );
    return str;
  }

  setLike ( item ) {
    let data = {
      accountStr: this.getAccountStr(),
      id: item[ 'id' ],
    };
    axios.post( `${API_URL}/setLike` ,  data )
    .then(function (response) {
    })
    .catch(function (error) {
    	Alert( error.response.data[ 0 ] , 'error' );
      	console.log({error});
    });
  }


  refreshItems() {
    let self = this;
    axios.get( `${API_URL}/getQueue/${this.getAccountStr()}` )
    .then( function ( response ) {
      self.setState({ items: response[ 'data' ] });
      // console.log({response});
    })
    .catch(function (error) {
      console.log({error});
    });
  }
  componentWillUnmount () {
  	clearInterval( this.interval );
  }
  componentDidMount () {
    this.refreshItems();
    this.interval = setInterval( () => this.refreshItems() , 2500 );
    let accountTitle = localStorage.getItem( 'accountTitle' ) || '';
    if ( accountTitle.length > 5 ) {
      this.setState({
        accountTitle
      });
    }
    // let res = await fetch(  );
    // let resp = await res.json();
    // let items = resp.items;
    // console.log(items)
  }
  render() {
    return (
      <div className="container-fluid">
        <h1>{ this.state.accountTitle }</h1>
        <h3>Queue</h3>
        <table className="table table-striped table-bordered">
          <tbody>
            {
              this.state.items.map( ( item , index ) => {
                return (
                  <tr key={ index }>
                    <td>
                      <p>{ item.title }</p>
                      <span className="mr-2">{ item.likes }</span>
                      <button className="btn btn-primary btn-sm" onClick={ () => this.setLike( item ) }><i className="fa fa-thumbs-up"></i></button>
                    </td>
                    <td>
                      <img src={ item.thumbnail_url } alt={ item.title } height="60" />
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}
