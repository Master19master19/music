module.exports = function (api) {
    api.cache(true);
    return {
        presets: [],
        plugins: [
            ['module:react-native-dotenv'],
            [
                'babel-plugin-module-resolver',
                {
                    root: ['.'],
                    alias: {
                        '@app': './src',
                        '@img': './src/assets/images',
                        '@icons': './src/assets/images/icons',
                        '@components': './src/components',
                        '@constants': './src/constants',
                        '@screens': './src/screens',
                        '@hooks': './src/hooks',
                        '@actions': './src/actions',
                        '@reducers': './src/reducers',
                        '@utils': './src/utils',
                    },
                },
            ],
        ],
    };
};
