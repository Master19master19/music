const path = require('path');
module.exports = {
    resolve: {
        alias: {
            '@app': path.resolve(__dirname, './src'),
            '@utils': path.resolve(__dirname, './src/utils'),
            '@hooks': path.resolve(__dirname, './src/hooks'),
            '@img': path.resolve(__dirname, './src/assets/images'),
            '@components': path.resolve(__dirname, './src/components'),
            '@constants': path.resolve(__dirname, './src/constants'),
            '@icons': path.resolve(__dirname, './src/assets/images/icons'),
            '@screens': path.resolve(__dirname, './src/screens'),
        },
    },
};
